
import jade.core.AID;
import java.util.HashMap;
import java.util.Map;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author raulcancelas
 */
class Clasification{
    
    public HashMap<AID, PlayerInfo> players = new HashMap<>();
    public HashMap<String,String> matches = new HashMap<>();
    
    static int id = 1;
    
    String datos[][];
    

  public Clasification() {
  }
  
  public void setPlayers(int number, AID[] names){
        datos = new String[number][7]; 
        for (int x = 0; x < names.length; x++){
            players.put(names[x], new PlayerInfo(names[x],x));
        }
  }
  
  public void initializePlayers(){
        players.entrySet().forEach((entry) -> {
            entry.getValue().G = 0;
            entry.getValue().D = 0;
            entry.getValue().V = 0;
            entry.getValue().E = 0;
            entry.getValue().D = 0;
            entry.getValue().PJ = 0;
        });
    }
  
  
  public String[][] getDatos(){
      int x = 0;
      for (Map.Entry<AID, PlayerInfo> entry : players.entrySet()) {
        datos[x][0] = entry.getKey().getLocalName();
        datos[x][1] = entry.getValue().G+"";
        datos[x][2] = entry.getValue().P+"";
        datos[x][3] = entry.getValue().V+"";
        datos[x][4] = entry.getValue().E+"";
        datos[x][5] = entry.getValue().D+"";
        datos[x][6] = entry.getValue().PJ+"";
        x++;
    }
    return datos;
  }
  
    public void setResultado(String player1, int p1, String player2, int p2){
      for (Map.Entry<AID, PlayerInfo> entry : players.entrySet()) {
        if(entry.getKey().getLocalName().equals(player1)){
            entry.getValue().G += p1;
            entry.getValue().P += p2;
            if(p1 > p2) entry.getValue().V += 1;
            if(p1 < p2) entry.getValue().D += 1;
            if(p1 == p2) entry.getValue().E += 1;
            entry.getValue().PJ += 1;
        }
      
        
    }
       for (Map.Entry<AID, PlayerInfo> entry : players.entrySet()) {
        if(entry.getKey().getLocalName().equals(player2)){
            entry.getValue().G += p2;
            entry.getValue().P += p1;
            if(p1 < p2) entry.getValue().V += 1;
            if(p1 > p2) entry.getValue().D += 1;
            if(p1 == p2) entry.getValue().E += 1;
            entry.getValue().PJ += 1;
        }
    }
        matches.put(player1+"."+player2, p1+"-"+p2);
  }
    
    public HashMap<String,String> getMatches(){
        return matches;
    }
    
    public HashMap<String,String> getPlayers(){
        HashMap<String, String> play = new HashMap<>();
        for (Map.Entry<AID, PlayerInfo> entry : players.entrySet()) {
            play.put(entry.getKey().getLocalName(), entry.getKey().getLocalName());
        }
        return play;
    }
    
        public class PlayerInfo {

        AID aid;
        int id;
        int G;
        int E;
        int P;
        int V;
        int D;
        int PJ;

        public PlayerInfo(AID a, int i) {
            aid = a;
            id = i;
        }

        @Override
        public boolean equals(Object o) {
            return aid.equals(o);
        }
    }
    
}

