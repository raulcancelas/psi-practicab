
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.lang.acl.ACLMessage;
import java.util.Random;
/**
 *
 * @author raulcancelas
 */
public class RandomPlayer extends Agent {
    
    private Agent agent;
    
    private int state = 0;
    private AID mainAgent;
    private int myId, opponentId;
    private int N, S, R, I, P;
    private ACLMessage msg;

    
    protected void setup() {
       
	System.out.println("RANDOM AGENT RADY: " + getAID().getName());
        agent = this;
        
	DFAgentDescription dfd = new DFAgentDescription();
	ServiceDescription sd = new ServiceDescription();
	sd.setName(getLocalName());
	sd.setType("Player");
        sd.setName("Game");
	dfd.addServices(sd);
		
	try {
            DFService.register(this, dfd);
	} catch (FIPAException e) {
            e.printStackTrace();
	}
		
	System.out.println("Hello! Fixed-Agent: " + getAID().getName() + " is ready.");
		
        PlayGame playGame = new PlayGame();
        addBehaviour(playGame);
        
    }
    
    
    protected void takeDown(){
        try {
            DFService.deregister(this);
        } catch (FIPAException e) {
            e.printStackTrace();
        }
        System.out.println("RANDOM AGENT CLOSE:  " + getAID().getName());
    }
    
    
    
    public class PlayGame extends CyclicBehaviour {
        @Override
        public void action() {
            msg = blockingReceive();
            if(msg != null){
                switch(state){
                    case 0:
                        System.out.println(msg.getContent() + "------------------");
                        if (msg.getContent().startsWith("Id#") && msg.getPerformative() == ACLMessage.INFORM) {
                            boolean parametersUpdated = false;
                            try {
                                parametersUpdated = validateSetupMessage(msg);
                            } catch (NumberFormatException e) {
                                System.out.println(getAID().getName() + " - State 0 - Bad message");
                            }
                            System.out.println(parametersUpdated);
                            if (parametersUpdated) state = 1;

                        } else {
                            System.out.println(getAID().getName() + " - State 0 - Unexpected message");
                        }
                    break;

                    case 1:
                        System.out.println(msg.getContent() + "------------------");
                        if (msg.getContent().startsWith("NewGame#")) {
                            boolean gameStarted = false;
                            try {
                                gameStarted = validateNewGame(msg.getContent());
                            } catch (NumberFormatException e) {
                                System.out.println(getAID().getName() + " - State 1 - Bad message");
                            }
                            if (gameStarted) state = 2;
                        } else {
                            System.out.println(getAID().getName() + " - State 1 - Unexpected message");
                        }
                    break;
                
                    case 2:
                        System.out.println(msg.getContent() + "------------------");
                        if (msg.getPerformative() == ACLMessage.REQUEST && msg.getContent().startsWith("Position")) {
                            ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
                            msg.addReceiver(mainAgent);
                            
                            int randomdecision = (int)Math.floor(Math.random()*(N));
                            
                            msg.setContent("Position#" + randomdecision);
                            System.out.println(getAID().getName() + " sent " + msg.getContent());
                            send(msg);
                            state = 3;
                        } else if (msg.getPerformative() == ACLMessage.INFORM && msg.getContent().startsWith("Changed#")) {
                            // Process changed message, in this case nothing
                        } else if (msg.getPerformative() == ACLMessage.INFORM && msg.getContent().startsWith("EndGame")) {
                            state = 1;
                        } else {
                            System.out.println(getAID().getName() + " - State 2 - Unexpected message:" + msg.getContent());
                        }
                    break;

                    case 3:
                        System.out.println(msg.getContent() + "------------------");
                        //If INFORM RESULTS --> go to state 2
                        //Else error
                        if (msg.getPerformative() == ACLMessage.INFORM && msg.getContent().startsWith("Results#")) {
                            //Process results
                            state = 2;
                        } else {
                            System.out.println(getAID().getName() + " - State 3 - Unexpected message");
                        }
                    break;
                }
            }
        }

    }

    /**
     * Validates and extracts the parameters from the setup message
     *
     * @param msg ACLMessage to process
     * @return true on success, false on failure
     */
    private boolean validateSetupMessage(ACLMessage msg) throws NumberFormatException {
        int tN, tS, tR, tI, tP, tMyId;
        String msgContent = msg.getContent();

        String[] contentSplit = msgContent.split("#");
        if (contentSplit.length != 3) return false;
        if (!contentSplit[0].equals("Id")) return false;
        tMyId = Integer.parseInt(contentSplit[1]);
        String[] parametersSplit = contentSplit[2].split(",");
        if (parametersSplit.length != 5) return false;
        tN = Integer.parseInt(parametersSplit[0]);
        tS = Integer.parseInt(parametersSplit[1]);
        tR = Integer.parseInt(parametersSplit[2]);
        tI = Integer.parseInt(parametersSplit[3]);
        tP = Integer.parseInt(parametersSplit[4]);

        //At this point everything should be fine, updating class variables
        mainAgent = msg.getSender();
        N = tN;
        S = tS;
        R = tR;
        I = tI;
        P = tP;
        myId = tMyId;
        return true;
    }

    /**
     * Processes the contents of the New Game message
     * @param msgContent Content of the message
     * @return true if the message is valid
     */
    public boolean validateNewGame(String msgContent) {
        int msgId0, msgId1;
        String[] contentSplit = msgContent.split("#");
        if (contentSplit.length != 2) return false;
        if (!contentSplit[0].equals("NewGame")) return false;
        String[] idSplit = contentSplit[1].split(",");
        if (idSplit.length != 2) return false;
        msgId0 = Integer.parseInt(idSplit[0]);
        msgId1 = Integer.parseInt(idSplit[1]);
        if (myId == msgId0) {
            opponentId = msgId1;
            return true;
        } else if (myId == msgId1) {
            opponentId = msgId0;
            return true;
        }
        return false;
    }
}
