/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author raulcancelas
 */

class Matrix{

  int datos[][][];

  Matrix() {
    newMatrix(4);
  }

  public int newMatrix(int size){
    datos = new int[size][size][2];  
      
    for( int i=0; i < datos.length; i++ ) {
      for( int j=0; j < datos[0].length; j++ ){
        for( int z=0; z < datos[0][0].length; z++ ){
            datos[i][j][z] = (int) Math.floor(Math.random()*10);
        }
      }
    }
    
    //Hacerla simetrica
    for( int i=0; i < datos.length; i++ ) {
      for( int j=0; j < datos[0].length; j++ ){
        datos[i][j][1] = datos[j][i][0];
        datos[i][j][0] = datos[j][i][1];
      }
    }

    return 1;
  }


  public int[][][] getDatos(){
    return datos;
  }
  
  public int[] getDatos(int position1, int position2){
      return(datos[position1][position2]);
  }
}

