/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author raulcancelas
 */
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.SimpleBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class MainAgent extends Agent {
    
    
    static MainAgent my;
    static Window GUI;
    static Matrix matriz = new Matrix();
    static Clasification clasification = new Clasification();
    
    private int size;
    private int players;
    private int rounds;
    private int roundI;
    private int porcent;
    boolean newGame = false;
    
    private StartGame gameBehaviour;
    
    

    /***
     * Setup of MAIN AGENT
     * Initialize new game with defect params and search players
     */
    protected void setup() {
       
	System.out.println("MAIN AGENT RADY: " + getAID().getName());
        
        my = this;
        GUI = new Window(this);
        GUI.setVisible(true);
        
        size = GUI.getS();
        rounds = GUI.getR();
        roundI = GUI.getI();
        porcent = GUI.getP();
        players = Integer.parseInt(GUI.playersNumber.getText());
        
        menu();
        initialize();
        
        addBehaviour(new SearchPlayers(this));
    }
    
    /***
     * Exit Function
     */
    protected void takeDown(){
        System.out.println("MAIN AGENT CLOSE: " + getAID().getName());
    }

    
    /***
     * Create new game with new params.
     * No search players
     */
    public void NewGameBTN(){
        
        size = GUI.getS();
        rounds = GUI.getR();
        roundI = GUI.getI();
        porcent = GUI.getP();
        players = Integer.parseInt(GUI.playersNumber.getText());
        
        if(size < 0 || players < 0 || rounds < 0 || roundI < 0 || porcent < 0){
            GUI.setLog("\n*Las variables deben de ser ENTEROS mayores que 0.\n", Color.RED, true);
        }else{
            menu();
            initialize();
        }
    }
    
    /***
     * Initialize game
     */
    public void initialize(){
        matriz.newMatrix(size);
        GUI.setMatrix(matriz.getDatos());
    }
    
    /***
     * Print menu
     */
    public void menu(){
        GUI.setLog("\n\n----------------------------------", Color.BLACK, true);
        GUI.setLog("-------------NEW GAME-------------", Color.BLACK, true);
        GUI.setLog("----------------------------------\n", Color.BLACK, true);
        GUI.setLog("Create new matrix", Color.BLACK, true);     
        GUI.setLog("S="+size+"\tP="+players+"\tR="+rounds+"\tI="+roundI+"\tP="+porcent, Color.BLACK, true);
    }
    
    /**
     * Search new players
     */
    public void searchPlayersBTN(){
        addBehaviour(new SearchPlayers(this));
    }
    
    /***
     * START BUTTON
     */
    public void startBTN(){
        gameBehaviour = new StartGame();
        addBehaviour(gameBehaviour);
    }
    
    
    /***
     * SEARCH PLAYERS BEHAVIOUR
     */
    public class SearchPlayers extends SimpleBehaviour {
        
        public SearchPlayers(Agent a) {
            super(a);
        }

        public void action() {
            GUI.setLog("Searching agents", Color.GREEN, true);
				
            DFAgentDescription[] agents = null;
            try {
                DFAgentDescription template = new DFAgentDescription();
                ServiceDescription sd = new ServiceDescription();
                sd.setType("Player");
                template.addServices(sd);
		agents = DFService.search(my, template);
            } catch (Exception e) { 
                GUI.setLog("ERROR: searching agents\n", Color.RED, true);
            }	
            
            AID[] playerNames = new AID[agents.length];
            int x = 0;
            for (DFAgentDescription agent : agents) {
                GUI.setLog("JUGADOR: "+ agent.getName().getLocalName(), Color.BLACK, true);
                playerNames[x] = agent.getName();
                x++;
            }
            
            GUI.playersNumber.setText(""+x);
            players = Integer.parseInt(GUI.playersNumber.getText());
            clasification.setPlayers(x, playerNames);
            clasification.initializePlayers();
            GUI.setClasification(clasification.getDatos());
            GUI.setMatchList(clasification.getPlayers(), clasification.getMatches());
            GUI.setLog("Seteada clasificacion", Color.BLACK, false);
        }

        public boolean done() {
            return true;
        }

    }
    
    /*
    ++++++++++++++++++++++++++++++BEHAVIOURS++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    */
    
    /***
     * GAME LOGIC
     * 
     */
    public class StartGame extends CyclicBehaviour {
        
        int gameId = 0;
        int state = 0;
        int numberGames = 0;
        int actualGame = 0;
        
        HashMap<Integer,ArrayList<AID>> gameList = new HashMap<>();
        HashMap<Integer, AID> actualPlayers = new HashMap<>();
        
        public void action() {
            
             //INFORMA A TODOS LOS PARTICIPANTES
            if(state == 0){
                GUI.setLog("NEW GAMEs\n" , Color.ORANGE, true);
                GUI.gameBottons(false);
                numberGames = (players * (players - 1))/2;
                GUI.setLog("Number of games: " + numberGames, Color.BLACK, true);
                
                clasification.players.entrySet().forEach((entry) -> {
                    ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
                    msg.addReceiver(entry.getKey());
                    msg.setContent("Id#" + entry.getValue().id+"#"+players+","+size+","+rounds+","+roundI+","+porcent);
                    send(msg);
                    GUI.setLog("Send INFORM ("+entry.getKey().getLocalName()+"): "+ msg.getContent(), Color.BLUE, false);
                });
                state ++;
            }
            
            
            //CREA LISTA DE PARTIDOS
            int y = clasification.players.size();
            int x = clasification.players.size();
            if(state == 1){
                for (Clasification.PlayerInfo local : clasification.players.values()) {
                    for (Clasification.PlayerInfo visitor : clasification.players.values()) {
                        if(x < y){
                            GUI.setLog("PARTIDO: " + local.aid.getLocalName() +"vs"+ visitor.aid.getLocalName(), Color.BLUE, true);
                            playMatch(local, visitor);
                        }
                        x--;
                      }
                    x = clasification.players.size();
                    y--;
                }
                state++;
            }
            if(state == 2){
            removeBehaviour(gameBehaviour);
            GUI.gameBottons(true);
        }
        }
    }
    
    
    private int playMatch(Clasification.PlayerInfo player1, Clasification.PlayerInfo player2){
            //TODO assign player 1 and player 2
            ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
            msg.addReceiver(player1.aid);
            msg.addReceiver(player2.aid);
            msg.setContent("NewGame#" + player1.id + "," + player2.id);
            send(msg);
            GUI.setLog("Matrix: " + matriz.getDatos(), Color.BLUE, true);

            int pos1, pos2;
            int punt1 = 0 , punt2 = 0;

            for(int roundCount = 0; roundCount < rounds; roundCount++){
            
                msg = new ACLMessage(ACLMessage.REQUEST);
                msg.setContent("Position");
                msg.addReceiver(player1.aid);
                send(msg);

                GUI.setLog("Main Waiting for movement player 1: ", Color.BLUE, true);
                ACLMessage move1 = blockingReceive();
                GUI.setLog("Main Received" + move1.getContent() + " from " + move1.getSender().getName(), Color.BLUE, true);
                pos1 = Integer.parseInt(move1.getContent().split("#")[1]);

                msg = new ACLMessage(ACLMessage.REQUEST);
                msg.setContent("Position");
                msg.addReceiver(player2.aid);
                send(msg);
              
                GUI.setLog("Main Waiting for movement player 2: ", Color.BLUE , true);
                ACLMessage move2 = blockingReceive(); 
                GUI.setLog("Main Received " + move2.getContent() + " from " + move2.getSender().getName(), Color.BLUE, true);
                pos2 = Integer.parseInt(move2.getContent().split("#")[1]);

                msg = new ACLMessage(ACLMessage.INFORM);
                msg.addReceiver(player1.aid);
                msg.addReceiver(player2.aid);
                
                String payoff = ObtainPayoff(pos1,pos2);
                
                //TODO llevar la cuenta
                msg.setContent("Results#" + payoff);
                GUI.setLog("Sent : " + payoff, Color.RED, true);
                send(msg);
                
                punt1 += Integer.parseInt(payoff.split("#")[0]);
                punt2 += Integer.parseInt(payoff.split("#")[1]);

            }
                    
            msg.setContent("EndGame");
            send(msg);
            
            //Add result to clasification
            
            clasification.setResultado(player1.aid.getLocalName(), punt1, player2.aid.getLocalName(), punt2);
            GUI.setClasification(clasification.getDatos());
            GUI.setMatchList(clasification.getPlayers(), clasification.getMatches());
            GUI.setLog("Seteada clasificacion", Color.BLACK, false);
            
        return 1;
    }

    String ObtainPayoff(int position1, int position2){
        int[] payoff = matriz.getDatos(position1, position2);
        return payoff[0]+"#"+payoff[1];
    }
}
